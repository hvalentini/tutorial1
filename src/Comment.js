import React from 'react';
import Remarkable from 'remarkable';

var Comment = React.createClass({
  renderMarkup: function() {
    var md = new Remarkable();
    var rawMarkup = md.render(this.props.children.toString());
    return { __html: rawMarkup };
  },

  render: function() {
    const author = this.props.author || 'Anônimo';
    return (
      <div className="comment">
        <h2 className="commentAuthor">
          {author}
        </h2>
        <span dangerouslySetInnerHTML={this.renderMarkup()} />
      </div>
    );
  }
});

export default Comment;
