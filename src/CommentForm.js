import React from 'react';
import './CommentForm.css';

const style = {
  input: {
    display: 'block',
    marginBottom: '10px',
    width: '100%',
    border: '1px solid #333',
    padding: '10px',
    boxSizing: 'border-box',
  },
  button: {
    backgroundColor: '#2D64D1',
    color: '#FFF',
    cursor: 'pointer',
    border: 0,
    padding: '10px 15px',
    fontSize: '16px',
    fontWeight: 400,
  }
}


var CommentForm = React.createClass({
  getInitialState: function() {
    return {
      author: '',
      text: '',
    }
  },

  onChangeAuthor: function(e) {
    this.setState({ author: e.target.value })
  },

  onChangeText: function(e) {
    this.setState({ text: e.target.value })
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    if (!text || !author) {
      alert('É necessário informar Author e Text');
      return;
    }
    this.props.onCommentSubmit(this.state);
    this.setState({author: '', text: ''});

  },

  render: function() {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="Your name"
          style={style.input}
          value={this.state.author}
          onChange={this.onChangeAuthor}
        />
        <input
          type="text"
          placeholder="Say something..."
          style={style.input}
          value={this.state.text}
          onChange={this.onChangeText}
        />
      <button type="submit" style={style.button}>Comentar</button>
      </form>
    );
  }
});

export default CommentForm;
