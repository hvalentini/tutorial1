import React from 'react';
import Comment from './Comment.js';

var CommentList = React.createClass({
  renderComments: function() {
    return this.props.data.map((c, i) => {
      return (
        <Comment author={c.author} key={c.id}>
          {c.text}
        </Comment>
      )
    });
  },

  render: function() {
    return (
      <div className="commentList">
        {this.renderComments()}
      </div>
    );
  }
});

export default CommentList;
